--[[
  File: bullet.lua
  Date: 24-02-2014
  Author: MrCiastexx
  
  Description:
    Bullet logic file.
]]--

Bullet = class:new()

Bullet.x = 0
Bullet.y = 0

Bullet.velocity = 500
Bullet.lifetime = 0

Bullet.active = false

function Bullet:init(ttl)
  self.image = love.graphics.newImage("_assets/gfx/tank/bullet.png")
end

function Bullet:shoot(turret)
  self.x = turret.x
  self.y = turret.y
  
  self.ox = self.image:getWidth() / 2
  self.oy = self.image:getHeight() / 2
  
  self.angle = turret.angle
  self.active = true
end

function Bullet:update(dt)
  self.lifetime = self.lifetime + 20 * dt

  self.y = self.y - math.cos(self.angle) * (self.velocity * dt)
  self.x = self.x + math.sin(self.angle) * (self.velocity * dt)
end

function Bullet:draw(dt)
  love.graphics.draw(self.image, self.x, self.y, 0, 1, 1, self.ox, self.oy)
end

function Bullet:kill()
  self.x = 0
  self.y = 0
  self.lifetime = 0
  self.active = false
end