--[[
  File: tank.lua
  Date: 22-02-2014
  Author: MrCiastexx
  
  Description:
    Tank logic.
]]--
require("Actors.Tank.turret")

Tank = class:new()

-- Location
Tank.x = 0
Tank.y = 0

-- Because of physics
Tank.accel = 0
Tank.rotSpeed = 0
Tank.maxSpeed = 80
Tank.maxRearSpeed = 50
Tank.velocity = 0
Tank.angle = 0
Tank.torqueFactor = 1.6
Tank.engineForce = 1.5

-- Tank's uber perks
Tank.maxBoost = 150
Tank.availableBoost = 100
Tank.boostMultiplier = 1.7
Tank.boostDrainage = 20
Tank.accelBoost = 65

Tank.health = 100
Tank.armor = 100

Tank.maxArmor = 200
Tank.maxHealth = 150

-- Some conditionals
Tank.moving = { }
Tank.moving.up = false
Tank.moving.down = false

Tank.boosting = false
Tank.fired = false

-- Some properties
Tank.height = 0
Tank.width = 0

-- Constructor, requires initial X, Y, acceleration and rotation speed.
function Tank:init(x, y, accel, rotSpeed)
  self.x = x
  self.y = y
  
  self.accel = accel or 50
  self.rotSpeed = rotSpeed or 2
  
  local tankImage = love.graphics.newImage("_assets/gfx/tank/tankbase.png")
  self.animation = newAnimation(tankImage, 28, 28, 0.04, 2)
  self.height = tankImage:getHeight()
  self.width = tankImage:getWidth() / 2
  
  self.turret = Turret:new(self)
end

 function Tank:update(dt)
  -- Forward movement.
  if love.keyboard.isDown("up") then
    if self.boosting then
      -- We are boosting: Amplify acceleration and maximum speed.
      self.accel = self.accelBoost
      if self.velocity >= self.maxSpeed * self.boostMultiplier then
        -- Max boost speed reached? Cool. Let's have it that way now.
        self.velocity = self.maxSpeed * self.boostMultiplier
      end
    end
    
    -- We aren't boosting, so revert all parameters to normal.
    if not self.boosting then
      self.accel = 50
      if self.velocity > self.maxSpeed then
        -- Max speed reached? Cool. Let's keep it that way.
        self.velocity = self.velocity - self.accel * self.boostMultiplier * dt
      end
    end
    
    if self.velocity < 0 then
      -- Are we still moving backwards? Let's brake it faster.
      self.velocity = self.velocity + self.accel * (self.torqueFactor + self.engineForce) * dt 
    end
    
    -- We are moving forward!
    self.velocity = self.velocity + self.accel * dt
    
    self.moving.down = false
    self.moving.up = true
  end
  
  -- Backwards movement.
  if love.keyboard.isDown("down") then
    if self.velocity <= -self.maxRearSpeed then
      -- Maximum backwards speed reached? Keep it up.
      self.velocity = self.velocity + self.accel * dt
    end
    
    if self.velocity > 0 then 
      -- Still moving forward? Let's brake it faster.
      self.velocity = self.velocity - self.accel * (self.torqueFactor + self.engineForce) * dt 
    end
    
    -- We are moving backwards!
    self.velocity = self.velocity - self.accel * dt
    
    self.moving.up = false
    self.moving.down = true
  end
  
  if love.keyboard.isDown("left") then
    -- Left rotation.
    self.angle = self.angle - (self.rotSpeed * dt)
    self.animation:update(dt)
  end
  
  if love.keyboard.isDown("right") then
    -- Right rotation.
    self.angle = self.angle + (self.rotSpeed * dt)
    self.animation:update(dt)
  end
  
  -- Holding space, boosting!
  if love.keyboard.isDown(" ") then
    if self.moving.up then
      -- Do we still have some UberFuel(TM)?
      if self.availableBoost > 0 then
        self.boosting = true
        -- Drain boost when using it.
        self.availableBoost = self.availableBoost - self.boostDrainage * self.boostMultiplier * dt
      else
        -- No UberFuel(TM) left. Sad. :C
        self.boosting = false
        self.availableBoost = 0
      end
    end
  end
  
  -- Some cheats, because why not? :D
  if love.keyboard.isDown("z") then
    self:refillBoost(self.maxBoost)
  end
  
  if love.keyboard.isDown("x") then
    self:heal(self.maxHealth)
  end
  
  if love.keyboard.isDown("c") then
    self:rearm(self.maxArmor)
  end
  
  if love.keyboard.isDown("v") then
    self:refillAmmo(self.turret.maxBulletCapacity)
  end
  
  -- Are we moving? If so, update position and make the tank 'move'.
  if self.moving.up or self.moving.down then     
    self.y = self.y - math.cos(self.angle) * (self.velocity * dt)
    self.x = self.x + math.sin(self.angle) * (self.velocity * dt)
    
    self.animation:update(dt)
  end
  
  -- Moving controls are not held. Handle it.
  if not love.keyboard.isDown("up") and not love.keyboard.isDown("down") then
    self.moving.up = false
    self.moving.down = false
    
    if self.velocity > 0 then
      -- Let's get the tank to stop by itself.
      self.velocity = self.velocity - self.accel * self.torqueFactor * dt
    elseif self.velocity < 0 then
      -- The same thing goes if we're moving backwards
      self.velocity = self.velocity + self.accel * self.torqueFactor * dt
    end
    
    -- Update position to reflect changes.
    self.y = self.y - math.cos(self.angle) * (self.velocity * dt)
    self.x = self.x + math.sin(self.angle) * (self.velocity * dt)
    
    -- Dirty hack to animate only when loose-moving and stop having random numbers at velocity.
    if (self.velocity >= 1) or (self.velocity <= -1) then
      self.animation:update(dt)
    else
      self.velocity = 0
    end
  end
  
  -- We are not boosting right now.
  if not love.keyboard.isDown(" ") then
    self.boosting = false
  end
  
  self.turret:update(dt)
end

function Tank:draw(dt)
  -- Draw ourselves.
  self.animation:draw(self.x, self.y, self.angle, 1, 1, 14, 14)
  self.turret:draw(dt)
end

function Tank:refillBoost(amount)
  if self.availableBoost + amount > self.maxBoost then
    self.availableBoost = self.maxBoost
  else
    self.availableBoost = self.availableBoost + amount
  end
end

function Tank:refillAmmo(amount)
  if self.turret.normalBulletReserves + amount > self.turret.maxBulletCapacity then
    self.turret.normalBulletReserves = self.turret.maxBulletCapacity
  else
    self.turret.normalBulletReserves = self.turret.normalBulletReserves + amount
  end
end

function Tank:heal(amount)
  if self.health + amount > self.maxHealth then
    self.health = self.maxHealth
  else
    self.health = self.health + amount
  end
end

function Tank:rearm(amount)
  if self.armor + amount > self.maxArmor then
    self.armor = self.maxArmor
  else
    self.armor = self.armor + amount
  end
end