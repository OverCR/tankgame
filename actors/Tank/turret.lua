--[[
  File: turret.lua
  Date: 24-02-2014
  Author: MrCiastexx
  
  Description:
    Tank turret logic.
]]--
require("Actors.Tank.bulletManager")

Turret = class:new()

-- Position
Turret.x = 0
Turret.y = 0
Turret.ox = 0
Turret.oy = 0

-- Again, maths...
Turret.angle = 0
Turret.rotSpeed = 2

-- Ammo reserves
Turret.normalBulletReserves = 30

-- Ammo capacity
Turret.maxBulletCapacity = 60

-- Constructor, requires tank's base.
function Turret:init(base)
  self.base = base
  self.image = love.graphics.newImage("_assets/gfx/tank/tankturret.png")
  
  self.ox = self.image:getWidth() / 2
  self.oy = self.image:getHeight() / 2
  
  self.bulletManager = BulletManager:new(self, 5)
end

function Turret:update(dt)
  -- Pretty straightforward. Take base's X and Y then subtract some *cough*magic*cough* from them.
  self.x = self.base.x - self.base.width / 7 + 4
  self.y = self.base.y - self.base.height / 7 + 4
  
  if love.keyboard.isDown("q") then
    -- Rotate to the left...
    self.angle = self.angle - self.rotSpeed * dt
  elseif love.keyboard.isDown("e") then
    --- ...or to the right!
    self.angle = self.angle + self.rotSpeed * dt
  end
  
  -- We want a single keypress, for shooting, so we're using an event here.
  function love.keypressed(key)
    if key == "lctrl" then
      if self.normalBulletReserves > 0 then
        self.bulletManager:shoot()
        if self.bulletManager.canShoot then
          self.normalBulletReserves = self.normalBulletReserves - 1
        end
      end
    end
  end
  self.bulletManager:update(dt)
end

function Turret:draw(dt)
  self.bulletManager:draw(dt)
  love.graphics.draw(self.image, self.x, self.y, self.angle, 1, 1, self.ox, self.oy)
end