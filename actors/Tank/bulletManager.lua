--[[
  File: bulletManager.lua
  Date: 24-02-2014
  Author: MrCiastexx
  
  Description:
    BulletManager logic file.
]]--
require("Actors.Tank.bullet")

BulletManager = class:new()

BulletManager.bullets = { }
BulletManager.bulletTtl = 50

BulletManager.canShoot = true


-- Constructor, requires a turret and maximum bullets that could be shot at once.
function BulletManager:init(turret, maxBullets)
  self.turret = turret
  self.maxBullets = maxBullets
  
  for i=1,maxBullets do
    -- Load the clip!
    self.bullets[i] = Bullet:new(self.bulletTtl)
  end
end

function BulletManager:update(dt)
  for i,v in ipairs(self.bullets) do
    -- We update only active bullets.
    if v.active then
      -- And those, which only live.
      if(v.lifetime <= self.bulletTtl) then
        v:update(dt)
      else
        -- Kill bullet after it exceeds its maximum lifetime.
        v:kill()
      end
    end
  end
end

function BulletManager:draw(dt)
  for i, v in ipairs(self.bullets) do
    -- We draw only active bullets.
    if v.active then
      v:draw(dt)
    end
  end
end

function BulletManager:shoot()
  -- Get a free bullet we have.
  bullet = self.bullets[self:__getInactiveBullet()]
  
  -- If there's found, then shoot.
  if self.canShoot then
    -- Tell the bullet that it can get out of barrel.
    bullet:shoot(self.turret)
    bullet.active = true
  end
end

--[[ INTERNAL FUNCTION ]]--
function BulletManager:__getInactiveBullet()
  -- Iterate through all bullets (Y U NO HAZ LINQ, LUA)
  for i=1,self.maxBullets do
    -- Found inactive?
    if self.bullets[i].active == false then
      -- Can shoot, return our finding.
      self.canShoot = true
      return i
    else
      -- Nope. No free bullets found.
      self.canShoot = false
    end
  end
end