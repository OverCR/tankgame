--[[
  File: conf.lua
  Date: 22-02-2014
  Author: MrCiastexx
  
  Description:
    Configuration file.
]]--

function love.conf(t)
  t.window.title = "Czoug"
  t.window.width = 640
  t.window.height = 480
  t.window.resizable = true
  
  
  t.version = "0.9.0"
  t.console = true
  
  
  t.modules.joystick = false
end