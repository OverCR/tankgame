--[[
  File: main.lua
  Date: 22-02-2014
  Author: MrCiastexx
  
  Description:
    Main program file.
]]--

--[[

]]--

require("Core.secs")
require("Core.anal")

require("Actors.Tank.tank")

require("Interface.gameHud")

function love.load(arg) 
  if arg[#arg] == "-debug" then require("mobdebug").start() end
  -- Actors
  tank = Tank:new(40, 40, 50, 2)
  gameHud = GameHud:new(tank)
end

function love.update(dt)
  tank:update(dt)
end

function love.draw(dt)
  tank:draw(dt)
  gameHud:draw(dt)
end