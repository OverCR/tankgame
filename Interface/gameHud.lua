--[[
  File: gameHud.lua
  Date: 25-02-2014
  Author: MrCiastexx
  
  Description:
    HUD logic file.
]]--

GameHud = class:new()

GameHud.font = love.graphics.newFont("_assets/fonts/hud.ttf", 16)

function GameHud:init(tank)
  self.tank = tank
end

function GameHud:draw(dt)
  love.graphics.setFont(self.font)
  -- love.graphics.printf("Velocity: " .. self.tank.velocity, 2, 2, 1000)  
  love.graphics.printf("FPS: " .. love.timer.getFPS(), 5, 10, 1000)
  
  -- Tank stats
  love.graphics.printf("Boost: " .. self.tank.availableBoost, 453, 456, 1000)
  love.graphics.printf("Ammo: " .. self.tank.turret.normalBulletReserves .. "/" .. tank.turret.maxBulletCapacity, 453, 430, 1000)
  love.graphics.printf("Health: " .. self.tank.health .. "/" .. self.tank.maxHealth .. "%", 10, 430, 1000)
  love.graphics.printf("Armor: " .. self.tank.armor .. "/" .. self.tank.maxArmor .. "%", 10, 456, 1000)
end